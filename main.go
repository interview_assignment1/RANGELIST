package main

import (
	"errors"
	"fmt"
)

//区间Section结构体
type Section struct {
	leftIdx  int
	rightIdx int
}

//区间列表RangeList结构体
type RangeList struct {
	RangeListDict []Section
}

/*
功能：初始化区间列表
*/
func Constructor() RangeList {
	return RangeList{
		RangeListDict: []Section{},
	}
}

/*
功能：向区间列表中添加新区间
参数：区间对象section
*/
func (rangelist *RangeList) Add(section Section) error {
	left := section.leftIdx
	right := section.rightIdx
	length := len(rangelist.RangeListDict)
	var maxEle (int)
	var minEle (int)
	//异常处理：区间范围不合法
	if left > right {
		return errors.New("Range Element Error.")
	}
	//特殊情况：区间列表为空
	if length == 0 {
		rangelist.RangeListDict = append(rangelist.RangeListDict, section)
		return nil
	} else {
		//当前区间列表的边界
		for _, ele := range rangelist.RangeListDict {
			if ele.rightIdx > maxEle {
				maxEle = ele.rightIdx
			}
			if ele.leftIdx < minEle {
				minEle = ele.leftIdx
			}
		}
		if section.leftIdx > maxEle {
			//待插入区间大于最右边界
			rangelist.RangeListDict = append(rangelist.RangeListDict, section)
		} else if section.leftIdx == maxEle {
			//待插入区间和最右边届重合
			rangelist.RangeListDict[length-1].rightIdx = section.rightIdx
		} else if section.rightIdx < minEle {
			//待插入区间小于最左边界
			rangelist.RangeListDict = append(rangelist.RangeListDict, section)
		} else if section.rightIdx == minEle {
			//待插入区间和最左边界重合
			rangelist.RangeListDict[length-1].rightIdx = section.rightIdx
		} else {
			//遍历循环插入
			for i := 0; i < length; i++ {
				//待插入区间在已有区间内部
				if section.leftIdx > rangelist.RangeListDict[i].leftIdx && section.rightIdx < rangelist.RangeListDict[i].rightIdx {
					return nil
				}
				//待插入区间跨区间
				if section.leftIdx > rangelist.RangeListDict[i].leftIdx && section.rightIdx > rangelist.RangeListDict[i].rightIdx {
					rangelist.RangeListDict[i].rightIdx = section.rightIdx
				}
			}
		}
	}
	return nil
}

/*
功能：删除区间列表中的指定区间
参数：区间对象section
*/
func (rangelist *RangeList) Remove(section Section) error {
	length := len(rangelist.RangeListDict)
	//异常处理：范围区间为空
	if length == 0 {
		return errors.New("No Element.")
	}
	//异常处理：待删除区间在已有范围外
	if section.rightIdx < rangelist.RangeListDict[0].leftIdx || section.leftIdx > rangelist.RangeListDict[length-1].rightIdx {
		return errors.New("Invaid Range.")
	}
	//遍历循环删除
	for i := 0; i < length; i++ {
		//超过当前区间
		if section.leftIdx > rangelist.RangeListDict[i].rightIdx {
			continue
		}
		//左边界小于等于,右边界小于
		if section.leftIdx <= rangelist.RangeListDict[i].leftIdx && section.rightIdx < rangelist.RangeListDict[i].rightIdx {
			rangelist.RangeListDict[i].leftIdx = section.rightIdx
			return nil
		}
		//包含区间
		if section.leftIdx <= rangelist.RangeListDict[i].leftIdx && section.rightIdx >= rangelist.RangeListDict[i].rightIdx {
			rangelist.RangeListDict[i].leftIdx = section.rightIdx
			return nil
		}
		//待删除区间在已有区间内部
		if section.leftIdx > rangelist.RangeListDict[i].leftIdx && section.rightIdx < rangelist.RangeListDict[i].rightIdx {
			newSection := Section{section.rightIdx, rangelist.RangeListDict[i].rightIdx}
			rangelist.RangeListDict = append(rangelist.RangeListDict, newSection)
			rangelist.RangeListDict[i].rightIdx = section.leftIdx
			return nil
		}
		//待删除区间跨越多个区间
		if section.leftIdx < rangelist.RangeListDict[i].rightIdx && section.rightIdx > rangelist.RangeListDict[i].rightIdx {
			max := rangelist.RangeListDict[length-1].rightIdx
			rangelist.RangeListDict = rangelist.RangeListDict[0:1]
			rangelist.RangeListDict[0].rightIdx = section.leftIdx

			newSection := Section{section.rightIdx, max}
			rangelist.RangeListDict = append(rangelist.RangeListDict, newSection)
			break
		}
	}
	return nil
}

/*
功能：打印当前区间列表对象中的区间
*/
func (rangelist *RangeList) Print() error {
	length := len(rangelist.RangeListDict)
	//当区间列表为空时，抛出异常；当区间列表存在数据时，正常输出
	if length == 0 {
		return errors.New("Empty RangeList.")
	} else {
		for i := 0; i < length; i++ {
			fmt.Print("[ ", rangelist.RangeListDict[i].leftIdx, " , ", rangelist.RangeListDict[i].rightIdx, " ) ")
		}
		fmt.Print("\n")
	}
	return nil
}

func main() {
	//fmt.Println("hello world2")
	rl := RangeList{}
	rl.Add(Section{1, 5})
	rl.Print()
	//    Should display: [1, 5)
	rl.Add(Section{10, 20})
	rl.Print()
	// Should display: [1, 5) [10, 20)
	rl.Add(Section{20, 20})
	rl.Print()
	// Should display: [1, 5) [10, 20)
	rl.Add(Section{20, 21})
	rl.Print()
	// Should display: [1, 5) [10, 21)
	rl.Add(Section{2, 4})
	rl.Print()
	// Should display: [1, 5) [10, 21)
	rl.Add(Section{3, 8})
	rl.Print()
	// Should display: [1, 8) [10, 21)
	rl.Remove(Section{10, 10})
	rl.Print()
	// Should display: [1, 8) [10, 21)
	rl.Remove(Section{10, 11})
	rl.Print()
	//Should display: [1, 8) [11, 21)
	rl.Remove(Section{15, 17})
	rl.Print()
	// Should display: [1, 8) [11, 15) [17, 21)
	rl.Remove(Section{3, 19})
	rl.Print()
	// Should display: [1, 3) [19, 21)
}
