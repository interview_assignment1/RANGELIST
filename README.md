# Range List

## 内容列表

- [背景](#背景)
- [安装](#安装)
- [运行结果](#运行结果)
- [维护者](#维护者)
- [如何贡献](#如何贡献)
- [使用许可](#使用许可)

## 背景
在投递GitLab开发岗位时，按要求完成如下笔试题目：
1. 使用Rubby或者Go语言，实现一个区间列表RangeList，并将代码提交到GitLab。
2. 区间列表RangeList要求完成如下功能
	- 添加区间范围函数Add()
	- 删除区间范围函数Remove()
	- 打印当前区间范围内的数据值
3. 时间复杂度和空间复杂度
	- Add():时间复杂度：${O(n)}$，空间复杂度${O(1)}$
	- Remove():时间复杂度：${O(n)}$，空间复杂度${O(1)}$
	- Print():时间复杂度：${O(n)}$，空间复杂度${O(1)}$
4. 优化思路
	采用二分查找定位到相应的范围区间，平均时间复杂度可以降低到${O(\log n)}$,但是在最坏情况下时间复杂度仍然为${O(n)}$

## 安装

这个项目使用 golang 语言开发。请确保你本地安装了语言的相关依赖。

```sh
$ git clone https://gitlab.com/interview_assignment1/RANGELIST.git
```
## 运行结果
![avatar](/imgs/Output.JPG)
## 维护者

[@SweatCoder](https://gitlab.com/SweatCoder)。

## 如何贡献

非常欢迎你的加入！[提一个 Issue](https://gitlab.com/interview_assignment1/RANGELIST/-/issues) 或者提交一个 Pull Request。


标准 Readme 遵循 [Contributor Covenant](http://contributor-covenant.org/version/1/3/0/) 行为规范。


## 使用许可

[MIT](LICENSE) © Richard Littauer
